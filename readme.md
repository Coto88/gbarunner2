Branch: Pok�mon Emerald. Made Pok�mon Emerald work in DLDI ARM7 by Coto.

Saving status: Saves implemented properly on SD cards (no corruption at all). Still requires GBA games to be SRAM-patched for now.

Note:
This branch uses an older GBARunner2 codebase and improvements are added on top of that. Games will likely work good or not at all as compared to GBARunner2 branch by Gericom.
The purpose of this branch is to have another GBARunner2 "from another point of view" and may aid more in researching bugs/features.

Coto.

-

Usage:
0) Format any SD/SDHC card: FAT32K 4K Sector Size
1) Create a /gba folder in SD root.
2) Put a gba bios (filename --> bios.bin) in the /gba folder or root directory of your sd card.
3) Copy homebrew/ gba games in there, make sure the rom is sram patched if needed.
4) pick either gbarunner2.nds from /release/arm7dldi-ntr (DLDI ARM7) or /release/arm9dldi-ntr (DLDI ARM9). I recommend DLDI ARM7 since it works in SD/SDHC cards and (could) in nDSi

Note: By default it is recommended to define #define ARM7_DLDI in shared.h and recompile. 
Otherwise, ARM9 DLDI doesn't work on NTR SDHC cards / and won't work in TWL mode. 

Devs:

/gbarunner2 folder comes from: 	https://github.com/Gericom/GBARunner2/tree/596fca323d081855791495498f85a32f5d9e7806
being sound fifo experiment branch, and boots pokemon emerald properly + sound fifo emulation works very well. 

/tomerge-gbarunner2 folder has gbaemu4ds saving methods that didn't work when SAVERAM was @ VRAM.
Must be merged when we hit the milestone: "implement Flash 128K saving". See Objectives.

/compiler-environment devkitARM r45 environment required to build this specific gbarunner2 branch.

-

Objectives:
	OK:Already has proper SOUND FIFO emulation implemented (pokemon emerald instruments play correctly) + devkitARM version: r45, libnds at commit 4c3bf52 and libfat at commit 3375e8f. (both on github)
	OK:Implemented fatfs + implement SAV loading

	Todo:Implement Flash 128K saving
	Todo:Implement GB Sounds.

-

General project layout:

Data Abort:

r5 = pc address (pc + 8) that caused the data abort
r10 = opcode held by pc address that caused the data abort
r9 = Rn(base address) from within the pc address's opcode that generated the data abort


SD/SD Cache:
	If ARM9DLDI: runs DLDI entirely from ARM9 and uses VRAM B & C (256K) as scratchpad GBA rom memory (u8 * cluster_cache). 
	
	If ARM7DLDI: DLDI section(.vramdldi) is copied from EWRAM (because it can't be in VRAM as it was originally in GBARunner2 sources) as some loaders may not read the DLDI magic string properly:
		Thus the EWRAM DLDI section is grabbed by the DLDI relocator code (because DLDI needs to be relocated to run in any memory addresses other than the last one adjusted by DLDI code, 
		as told by "Offsets to important sections within the data	-- 32 bytes" start / end addresses dictated by the dldi_stub.s), into ARM7 target DLDI section (.dldi7), then all DLDI code is ran
		from there.
		
	VRAM D maps the clusters a GBA file has (gba_rom_cluster_table), an ARM7/ARM9 shared buffer (arm9_transfer_region), 
	and a cluster table from the u8 * cluster_cache to speed up seeking if GBA memory does not require to be reloaded, but still seeked by the GBA fetch operation.

	//SD driver data required is then filled from fatfs FILE * handle/FATFS structure:
	vram_cd->sd_info.first_cluster_sector = relative to GBAFILE Start clusterSector (cluster)
	vram_cd->sd_info.nr_sectors_per_cluster = should always be 512, but otherwise be read from SD descriptor
	..among others

Notes:
	read_sd_sector( sectorNum, numSector, bufferOut )

	vram_cd->gba_rom_cluster_table:
	physical SD FAT gbaFileHandle clusters starting from an absolute cluster 0 onwards.

	Makes relative offset (0) from GBA address then fetchs sector to VRAM C SD CACHE block (u8 cluster_cache[128K]) and retrieves GBAROM data:
	-u32 sdread32_uncached(uint32_t address)
	-u16 sdread16_uncached(uint32_t address)
	-u8 sdread8_uncached(uint32_t address)

	u32 cluster_index = current cluster relative to GBA ROM
	ensure_cluster_cached(uint32_t cluster_index)
	
		1) gets a new cache block
		// 128K (VRAM C) / 512 (SD sector size) = 256 cache blocks
		int get_new_cache_block() (CACHE_STRATEGY_ROUND_ROBIN)
		
		2) fills a VRAM C cluster_cache cluster from SD

		3) //returns the block relative to that cluster in VRAM C
	
-

Sound FIFO emulation:
ARM7:
main -> handles incoming SOUND FIFO buffer writes from GBA Virt (ASYNC)

	- GBA Virt Timer writes -> gba_sound_timer_updated(u16 gbatimerfreq)
	
TIMER3 IRQ -> handles SOUND FIFO frequency updater intervals (emulates GBA SOUND FIFO) so the samples are output into u8* soundBuffer

		-> then the sample is marked to start inmediately through gba_sound_update_ds_channels() into ARM7's sound card

ARM7 DLDI and ARM9 DLDI current VRAM format:
0x06800000 -> Vram A : Contiguous r/w/e hypervisor code (128K)

0x06820000 -> Vram B : Contiguous r/w/e hypervisor code (128K)

0x06840000 -> Vram C : SD CACHE. physical sector fetch/read/writes buffer of which is sliced in "blocks".

0x06860000 -> Vram D : GBA SD Driver. Allocates "blocks" and basically maps a GBAROM dinamically (GBARunner2 uses this to stream GBA file). Uses and keeps track of each block allocated in Vram C.

0x023E0000 -> 128K SAVERAM (Flash/Eeprom/Sram/etc)

0x23D8000 -> DLDI ARM7/ARM9 shared buffer.

Saving:
	ARM7:
	TIMER1 IRQ alters state and sends back response to ARM9 to save
	
	ARM9:
	saves when ARM7 says so
