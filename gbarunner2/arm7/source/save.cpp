#include <nds.h>
#include "timer.h"
#include "save.h"
#include "shared.h"
#include "dldigba.h"
#include <nds/bios.h>

void saveHandle(){
	if (!saveWorkCached->save_enabled){
		//invoke an irq on arm9
		*((vu32*)0x04000180) |= (1 << 13);
			
		return;
	}
	switch (saveWorkCached->save_state)
	{
		case SAVE_WORK_STATE_CLEAN:
			break;
		case SAVE_WORK_STATE_DIRTY:
			saveWorkCached->save_state = SAVE_WORK_STATE_SDSAVE;
			break;
		case SAVE_WORK_STATE_SDSAVE:{
			REG_SEND_FIFO = 0xFFFFFF00;
		}
		break;
	}
}

void gba_save_init()
{
	saveWorkCached->save_enabled = FALSE;
	saveWorkCached->save_state = SAVE_WORK_STATE_CLEAN;
}
