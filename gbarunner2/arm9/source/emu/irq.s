.section .itcm

#include "consts.s"


//inbetween to catch the current running function in usermode
.global irq_handler
irq_handler:
	STMFD   SP!, {R0-R3,R12,LR}

	//check for arm7 interrupt

	//make use of the backwards compatible version
	//of the data rights register, so we can use 0xFFFFFFFF instead of 0x33333333
	mov r0, #0xFFFFFFFF
	mcr p15, 0, r0, c5, c0, 0

	mov r12, #0x04000000

	ldr r1, [r12, #0x214]
	tst r1, #(1 << 16)
	bne irq_handler_arm7_irq

	ldr r1,= pu_data_permissions
	mcr p15, 0, r1, c5, c0, 2

	ADR     LR, loc_138
	LDR     PC, [R12,#-4]
loc_138:
	LDMFD   SP!, {R0-R3,R12,LR}
	SUBS    PC, LR, #4

irq_handler_arm7_irq:
	//do stuff
1:
	ldr r1, [r12, #0x184]
	tst r1, #(1 << 8)
	bne 1b
2:
	mov r0, #0x04100000
	ldr r1, [r0]	//read word from fifo
	
	cmp r1,#0xFFFFFF00
	beq do_save
	
	//cmp r1, #0x000000A5
	//beq irq_handler_arm7_irq_masterbright
	//cmp r1, #0x000000A6
	//beq irq_handler_arm7_irq_masterbright2

	//wait for fifo empty to prevent overflow
3:
	ldr r0, [r12, #0x184]
	tst r0, #1
	beq 3b

	ldr r0,= 0xAA5500F9
	str r0, [r12, #0x188]

	ldmia r1!, {r0, r2, r3, lr}
	
	str r0, [r12, #0x188]
	str r2, [r12, #0x188]
	str r3, [r12, #0x188]
	str lr, [r12, #0x188]

//4:
	ldr r1, [r12, #0x184]
	tst r1, #(1 << 8)
	beq 2b
	
	//acknowledge
fifo_leave:
	mov r1, #(1 << 16)
	str r1, [r12, #0x214]

	ldr r2,= fake_irq_flags
	ldr r1, [r2]
	orr r1, #(3 << 9)
	str r1, [r2]

	ldr r1,= pu_data_permissions
	mcr p15, 0, r1, c5, c0, 2
	LDMFD   SP!, {R0-R3,R12,LR}
	SUBS    PC, LR, #4

do_save:
	push {r0-r3,r10,r12,lr}
	ldr r1,=sd_write_save
	blx r1
	pop {r0-r3,r10,r12,lr}
	b fifo_leave