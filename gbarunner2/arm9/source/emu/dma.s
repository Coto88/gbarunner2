.section .itcm




#include "shared.h"

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ reads @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@DMA reads use fully: read_address_nomod_8 / read_address_nomod_16 / read_address_nomod_32 direct DMA reads (which skips hypervisor DMA handling at all)





@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ writes @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

.global write_address_dma_src
write_address_dma_src:
	cmp r11, #0x08000000
	blt write_address_dma_src_cont
	cmp r11, #0x0E000000
	bge write_address_dma_src_cont2
	bic r11, r11, #0x07000000
	sub r11, r11, #0x05000000
	sub r11, r11, #0x00FC0000
	str r11, [r9]
	bx lr
write_address_dma_src_cont:
	ldr r13,= DISPCNT_copy
	ldrh r13, [r13]
	and r13, #7
	cmp r13, #3
	ldrlt r13,= 0x06010000
	ldrge r13,= 0x06014000
	cmp r11, r13
	blt write_address_dma_src_cont2
	ldr r13,= 0x06018000
	cmp r11, r13
	addlt r11, #0x3F0000
write_address_dma_src_cont2:
	str r11, [r9]
	bx lr

.global write_address_dma_src_top16
write_address_dma_src_top16:
	cmp r11, #0x0800
	blt write_address_dma_src_cont_top16
	cmp r11, #0x0E00
	bge write_address_dma_src_cont_top16_2
	bic r11, r11, #0x0700
	sub r11, r11, #0x0500
	sub r11, r11, #0x00FC
	strh r11, [r9]
	bx lr
write_address_dma_src_cont_top16:
	ldr r13,= 0x0601
	cmp r11, r13
	addeq r11, #0x3F
write_address_dma_src_cont_top16_2:
	strh r11, [r9]
	bx lr

.global write_address_dma_dst
write_address_dma_dst:
	ldr r13,= DISPCNT_copy
	ldrh r13, [r13]
	and r13, #7
	cmp r13, #3
	ldrlt r13,= 0x06010000
	ldrge r13,= 0x06014000
	cmp r11, r13
	blt write_address_dma_dst_cont
	ldr r13,= 0x06018000
	cmp r11, r13
	addlt r11, #0x3F0000
write_address_dma_dst_cont:
	str r11, [r9]
	bx lr

.global write_address_dma_dst_top16
write_address_dma_dst_top16:
	ldr r13,= 0x0601
	cmp r11, r13
	addeq r11, #0x3F
	strh r11, [r9]
	bx lr

.global write_address_dma_size
write_address_dma_size:
	ldr r13,= 0x040000DC
	cmp r9, r13
	streqh r11, [r9]
	bxeq lr
	cmp r11, #0
	moveq r11, #0x4000
	strh r11, [r9]
	bx lr

.global write_address_dma_control
write_address_dma_control:
	bic r11, r11, #0x1F
	ldr r13,= 0x040000DE
	cmp r9, r13
	ldreqh r13, [r9, #-2]
	cmpeq r13, #0
	orreq r11, #1
	mov r13, r11, lsr #12
	and r13, #3
	cmp r13, #3
	bge write_address_dma_control_cont
	bic r11, r11, #0x3800
	orr r11, r11, r13, lsl #11

	tst r11, #0x8000
	beq write_address_dma_control_cont2
	ldr r13, [r9, #-0x6]
	cmp r13, #0x02000000
	biclt r11, #0x8000
	blt write_address_dma_control_cont2

	ldr r12,= MAIN_MEMORY_ADDRESS_SAVE_DATA	@guard protect ROM code

	ldrh r10, [r9, #-0x2]
	and r13, r11, #0x1F
	orr r10, r13, lsl #16
	tst r11, #(1 << 10)

	ldr r13, [r9, #-0xA]

	addeq r13, r13, r10, lsl #1
	addne r13, r13, r10, lsl #2

	cmp r13, r12
	blt write_address_dma_control_cont2
	cmp r13, #0x03000000
	blt write_address_dma_control_rom_src
	ldr r12,= 0x040000A4
	ldr r13, [r9, #-0x6]
	cmp r13, r12
	bxeq lr
write_address_dma_control_cont2:
	strh r11, [r9]
	bx lr

write_address_dma_control_rom_src:
	//still store the register, but not enabled
	bic r11, r11, #0x8000
	strh r11, [r9]
	ldr sp,= address_dtcm + (16 * 1024)
	push {r0-r9,lr}
	ldr r0, [r9, #-0xA]
	sub r0, #MAIN_MEMORY_ADDRESS_ROM_DATA
	ldrh r10, [r9, #-0x2]
	and r12, r11, #0x1F
	orr r10, r12, lsl #16
	tst r11, #(1 << 10)
	moveq r1, r10, lsl #1
	movne r1, r10, lsl #2
	ldr r2, [r9, #-0x6]
	bl read_gba_rom
	pop {r0-r9,lr}
	bx lr

write_address_dma_control_cont:
	ldr r13,= 0x40000C6
	cmp r9, r13

	bic r11, r11, #0x8000
	strh r11, [r9]
	bxne lr

	ldr r13,= 0x040000BC
	ldr r13, [r13]

	ldr r10,= 0x04000188
	ldr r12,= 0xAA5500F8
	str r12, [r10]
	str r13, [r10]

	bx lr

.global write_address_dma_size_control
write_address_dma_size_control:
	mov r12, r11, lsl #16
	movs r12, r12, lsr #16
	bne write_address_dma_size_control_cont
	ldr r13,= 0x040000DC		@40000DCh - DMA3CNT_L - DMA 3 Word Count (W) (16 bit, 1..10000h)
	cmp r9, r13
	movne r12, #0x4000
	moveq r12, #0x10000
write_address_dma_size_control_cont:
	ldr r13,= 0x1FFFFF
	bic r11, r13
	orr r11, r12
	mov r13, r11, lsr #28
	and r13, #3
	cmp r13, #3
	bge write_address_dma_size_control_cont2
	bic r11, r11, #0x38000000
	orr r11, r11, r13, lsl #27
	tst r11, #0x80000000
	beq write_address_dma_size_control_cont3
	ldr r13, [r9, #-0x4]
	cmp r13, #0x02000000
	biclt r11, #0x80000000
	blt write_address_dma_size_control_cont3

	ldr r12,= MAIN_MEMORY_ADDRESS_SAVE_DATA	@guard protect ROM code

	ldr r13,= 0x1FFFFF
	and r10, r11, r13
	tst r11, #(1 << 26)

	ldr r13, [r9, #-0x8]

	addeq r13, r13, r10, lsl #1
	addne r13, r13, r10, lsl #2
	
	cmp r13, r12
	blt write_address_dma_size_control_cont3
	cmp r13, #0x03000000
	blt write_address_dma_size_control_rom_src
	ldr r12,= 0x040000A4
	ldr r13, [r9, #-0x4]
	cmp r13, r12
	bxeq lr
write_address_dma_size_control_cont3:
	str r11, [r9]
	bx lr


write_address_dma_size_control_rom_src:
	//still store the register
	bic r11, #0x80000000
	str r11, [r9]
	ldr sp,= address_dtcm + (16 * 1024)
	push {r0-r9,lr}
	ldr r0, [r9, #-0x8]
	sub r0, #MAIN_MEMORY_ADDRESS_ROM_DATA
	ldr r12,= 0x1FFFFF
	and r10, r11, r12
	tst r11, #(1 << 26)
	moveq r1, r10, lsl #1
	movne r1, r10, lsl #2
	ldr r2, [r9, #-0x4]
	bl read_gba_rom
	pop {r0-r9,lr}
	bx lr

write_address_dma_size_control_cont2:
	ldr r13,= 0x040000C4
	cmp r9, r13

	bic r11, #0x80000000
	str r11, [r9]
	bxne lr

	ldr r13,= 0x040000BC
	ldr r13, [r13]

	ldr r10,= 0x04000188
	ldr r12,= 0xAA5500F8
	str r12, [r10]
	str r13, [r10]

	bx lr
	
	

.pool