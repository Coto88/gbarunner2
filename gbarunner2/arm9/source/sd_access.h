#ifndef __SD_ACCESS_H__
#define __SD_ACCESS_H__

#include <nds.h>
#include "shared.h"
#include "ff.h"
#include "dldigba.h"

//This SD Cache driver works for large games (the filesystem layer is fatFS)
#define SD_CACHE_SIZE	(128 * 1024)

typedef struct
{
	uint32_t gba_rom_size;
	uint32_t cluster_shift;
	uint32_t cluster_mask;
	uint32_t access_counter;

	uint8_t nr_sectors_per_cluster;
	sec_t first_fat_sector;
	sec_t first_cluster_sector;
	uint32_t root_directory_cluster;
} sd_info_t;

typedef struct
{
	uint32_t counter	: 24;
	//uint32_t reserved	: 7;
	uint32_t counter2	: 7;
	uint32_t in_use		: 1;
	uint32_t cluster_index;
} cluster_cache_block_info_t;

typedef struct
{
	cluster_cache_block_info_t cache_block_info[256];
	uint32_t total_nr_cacheblocks;
} cluster_cache_info_t;

//vram config
typedef struct
{
	//vram c
	uint8_t cluster_cache[SD_CACHE_SIZE];
	//vram d
	uint32_t gba_rom_cluster_table[32 * 1024 / 4];// 32MB
	uint8_t gba_rom_is_cluster_cached_table[16 * 1024];	//allows roms up to 64MB
	struct
	{
		cluster_cache_info_t cluster_cache_info;
		sd_info_t sd_info;
	};
	
	#ifdef ARM9
	u8 tmpSector[512] __attribute__((aligned(32)));
	#endif
	
} vram_cd_t;

#define READ_U16_SAFE(addr)		(((uint8_t*)(addr))[0] | (((uint8_t*)(addr))[1] << 8))
#define READ_U32_SAFE(addr)		(((uint8_t*)(addr))[0] | (((uint8_t*)(addr))[1] << 8) | (((uint8_t*)(addr))[2] << 16) | (((uint8_t*)(addr))[3] << 24))

typedef struct
{
	char long_name[34];
	char short_name[12] __attribute__ ((aligned (4)));
	int is_folder;
} entry_names_t;

typedef enum
{
	SHORT_NAME = 0,
	LONG_NAME = 1
} SEARCH_TYPE;

#define vram_cd		((vram_cd_t*)sd_cluster_cache_memory)	//Contiguous VRAM 0x06840000 ~ 0x0687FFFF

#define SCREEN_COLS 32
#define SCREEN_ROWS 24
#define ENTRIES_START_ROW 2
#define ENTRIES_PER_SCREEN (SCREEN_ROWS - ENTRIES_START_ROW)
#define SKIP_ENTRIES (ENTRIES_PER_SCREEN/2 - 1)

static inline void MI_WriteByte(void* address, uint8_t value)
{
	u16 val = *((vu16*)((u32)address & ~1));

	if ((u32)address & 1)
		*((vu16*)((u32)address & ~1)) = (u16)(((value & 0xff) << 8) | (val & 0xff));
	else
		*((vu16*)((u32)address & ~1)) = (u16)((val & 0xff00) | (value & 0xff));
}


#endif

#ifdef __cplusplus
extern "C" {
#endif

extern char curFileChosen[256+1];
extern FATFS fatFs;
extern FIL fil;
extern DIR dir;
extern void dc_invalidate_range(void* start, uint32_t length);
extern void dc_flush_range(void*      start, uint32_t length);
extern void dc_flush_all();
extern void dc_invalidate_all();
extern void dc_wait_write_buffer_empty();

//extern sd_info_t gSDInfo;
extern int CreateLoadSave();
extern u32 __vram_start;

#ifdef __cplusplus
extern int comp_dir_entries(const entry_names_t* &dir1, const entry_names_t* &dir2);
#endif

extern uint32	getfhFirstCluster(FIL * filhdlrptr);
extern uint32	getfhNextCluster(FIL * filhdlrptr, int currCluster);
extern void read_gba_rom(uint32_t address, uint32_t size, uint8_t* dst);

extern void sd_write_save();

//usage:
//setBacklight(POWMAN_BACKLIGHT_TOP_BIT | POWMAN_BACKLIGHT_BOTTOM_BIT);	//both lit screens
//setBacklight(POWMAN_BACKLIGHT_TOP_BIT);								//top lit screen
//setBacklight(POWMAN_BACKLIGHT_BOTTOM_BIT);							//bottom lit screen
//setBacklight(0);														//non-lit both LCD screens (poweroff)
extern int	setBacklight(int flags);
extern void removeLastDirectory(char * inPath, char * outPath);

#ifdef __cplusplus
}
#endif
