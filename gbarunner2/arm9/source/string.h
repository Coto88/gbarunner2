#ifndef __STRING_H__
#define __STRING_H__

static inline int to_upper(const char c)
{
	return (c >= 'a' && c <= 'z') ? (c - ('a' - 'A')) : c;
}


#endif

#ifdef __cplusplus
extern "C" {
#endif

extern int strlen(const char* str);
extern char *strchr(const char *s, int c);
extern char *strrchr(const char *s, int c);
extern char *strpbrk(const char *s1, const char *s2);
extern int strcasecmp(const char* one, const char* another);
extern int strcmp(const char* one, const char* another);

extern void *memcpy ( void *dest, const void *src, size_t n);
extern int memcmp (const void * str1, const void * str2, int count);

#ifdef __cplusplus
}
#endif
