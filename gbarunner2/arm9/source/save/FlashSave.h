#pragma once
#include "Save.h"
#include "shared.h"

bool flash_patchV120(const save_type_t* type);
bool flash_patchV123(const save_type_t* type);
bool flash_patchV126(const save_type_t* type);
bool flash_patch512V130(const save_type_t* type);
bool flash_patch1MV102(const save_type_t* type);