#include <nds.h>
#include "shared.h"
#include "dldigba.h"
#include <string.h>

#ifdef ARM9
#include "sd_access.h"
#endif

#ifdef ARM7
//DLDI bits
#if (defined(__GNUC__) && !defined(__clang__))
__attribute__((optimize("O0")))
#endif

#if (!defined(__GNUC__) && defined(__clang__))
__attribute__ ((optnone))
#endif
u32 * DLDIARM7Address = NULL;
#endif


const uint32  DLDI_MAGIC_NUMBER = 
	0xBF8DA5ED;	

static const data_t dldiMagicString[] = "\xED\xA5\x8D\xBF Chishm";	// Normal DLDI file
static const data_t dldiMagicLoaderString[] = "\xEE\xA5\x8D\xBF Chishm";	// Different to a normal DLDI file

// Stored backwards to prevent it being picked up by DLDI patchers
const char DLDI_MAGIC_STRING_BACKWARDS [DLDI_MAGIC_STRING_LEN] =
	{'\0', 'm', 'h', 's', 'i', 'h', 'C', ' '} ;

static addr_t quickFind (const data_t* data, const data_t* search, size_t dataLen, size_t searchLen) {
	const int* dataChunk = (const int*) data;
	int searchChunk = ((const int*)search)[0];
	addr_t i;
	addr_t dataChunkEnd = (addr_t)(dataLen / sizeof(int));

	for ( i = 0; i < dataChunkEnd; i++) {
		if (dataChunk[i] == searchChunk) {
			if ((i*sizeof(int) + searchLen) > dataLen) {
				return -1;
			}
			if (memcmp (&data[i*sizeof(int)], search, searchLen) == 0) {
				return i*sizeof(int);
			}
		}
	}

	return -1;
}

#ifdef ARM9
const DLDI_INTERFACE* io_dldi_data = (const DLDI_INTERFACE*)&_dldi_start;
#endif

// 2/2 : once DLDI has been setup
struct DLDI_INTERFACE* dldiGet(void) {
	
	#ifdef ARM7
	struct DLDI_INTERFACE* arm7DLDI = (DLDI_INTERFACE*)DLDIARM7Address;
	return arm7DLDI;
	#endif
	
	#ifdef ARM9
	return &_io_dldi_stub;
	#endif

	#if defined(WIN32)
	return (struct DLDI_INTERFACE*)&_io_dldi_stub[0];
	#endif
	
}

static addr_t readAddr (data_t *mem, addr_t offset) {
	return ((addr_t*)mem)[offset/sizeof(addr_t)];
}

static void writeAddr (data_t *mem, addr_t offset, addr_t value) {
	((addr_t*)mem)[offset/sizeof(addr_t)] = value;
}

////////////////////////////////////////////// DLDI ARM7/ARM9 CODE ////////////////////////////////////////////// 

#ifdef ARM9
PUT_IN_VRAM
#endif
__attribute__ ((noinline)) bool dldi_handler_init()
{
	#ifdef ARM7_DLDI
	#ifdef ARM7
	struct DLDI_INTERFACE* dldiInit = (struct DLDI_INTERFACE*)DLDIARM7Address;
	if( (!dldiInit->ioInterface.startup()) || (!dldiInit->ioInterface.isInserted()) ){
		return false;
	}
	return true;
	#endif
	#endif
	
	#ifndef ARM7_DLDI
	//ARM9 only
	#ifdef ARM9
	FN_MEDIUM_STARTUP _DLDI_startup_ptr = (FN_MEDIUM_STARTUP)(*((uint32_t*)(0x06800000 + 0x60 + 0x8)));
	return _DLDI_startup_ptr();
	#endif
	
	#endif
}

#ifdef ARM9
PUT_IN_VRAM
#endif
__attribute__ ((noinline)) bool dldi_handler_read_sectors(sec_t sector, sec_t numSectors, void* buffer){
	#ifdef ARM7
	struct  DLDI_INTERFACE* dldiInterface = (struct DLDI_INTERFACE*)DLDIARM7Address;
	return dldiInterface->ioInterface.readSectors(sector, numSectors, buffer);
	#endif
	
	#ifdef ARM9
	FN_MEDIUM_READSECTORS _DLDI_readSectors_ptr = (FN_MEDIUM_READSECTORS)(*((uint32_t*)(0x06800000 + 0x60 + 0x10)));
	//void * buffer is in VRAM...
	//fetched sectors are in MAIN_MEMORY_ADDRESS_SDCACHE, must be dma'd back to buffer
	dc_flush_range((void*)MAIN_MEMORY_ADDRESS_SDCACHE, numSectors * 512);
	_DLDI_readSectors_ptr(sector, numSectors, (void*)MAIN_MEMORY_ADDRESS_SDCACHE);
	memcpy((uint16_t*)buffer, (uint16_t*)MAIN_MEMORY_ADDRESS_SDCACHE, (numSectors * 512) );
	#endif
	return true; 
}

#ifdef ARM9
PUT_IN_VRAM
#endif
__attribute__ ((noinline)) bool dldi_handler_write_sectors(sec_t sector, sec_t numSectors, const void* buffer){
	#ifdef ARM7
	struct  DLDI_INTERFACE* dldiInterface = (struct DLDI_INTERFACE*)DLDIARM7Address;
	return dldiInterface->ioInterface.writeSectors(sector, numSectors, buffer);
	#endif
	#ifdef ARM9
	FN_MEDIUM_WRITESECTORS _DLDI_writeSectors_ptr = (FN_MEDIUM_WRITESECTORS)(*((uint32_t*)(0x06800000 + 0x60 + 0x14)));
	//void * buffer is in VRAM...
	//fetched sectors are in MAIN_MEMORY_ADDRESS_SDCACHE, must be dma'd back to buffer
	dc_flush_range((void*)MAIN_MEMORY_ADDRESS_SDCACHE, numSectors * 512);
	memcpy((uint16_t*)MAIN_MEMORY_ADDRESS_SDCACHE, (uint16_t*)buffer, (numSectors * 512));
	_DLDI_writeSectors_ptr(sector, numSectors, (void*)MAIN_MEMORY_ADDRESS_SDCACHE);
	#endif
	return true;
}

////////////////////////////////////////////// DLDI ARM 9 CODE end ////////////////////////////////////////////// 


////////////////////////////////////////////// DLDI ARM 7 CODE ////////////////////////////////////////////// 

#ifdef ARM7_DLDI

//ARM9 resorts to callbacks between FIFO irqs to fecth DLDI data
#ifdef ARM9
//buffer should be in main memory
PUT_IN_VRAM __attribute__ ((noinline)) __attribute__((aligned(4)))	void read_sd_sectors_safe(sec_t sector, sec_t numSectors, void* buffer){
	//remote procedure call on arm7
	//assume buffer is in the vram cd block
	//uint32_t arm7_address = ((uint32_t)buffer) - ((uint32_t)vram_cd) + 0x06000000;
	//dc_wait_write_buffer_empty();
	//dc_invalidate_all();
	//dc_flush_range(vram_cd, 256 * 1024);
	//dc_flush_all();
	//map cd to arm7
	//REG_VRAMCNT_CD = VRAM_CD_ARM7;
	//REG_VRAMCNT_C = VRAM_C_ARM7;
	
	REG_SEND_FIFO = 0xAA5500DF;
	REG_SEND_FIFO = sector;
	REG_SEND_FIFO = numSectors;
	REG_SEND_FIFO = (uint32_t)MAIN_MEMORY_ADDRESS_SDCACHE;	//original: void * buffer -> arm7_address
	//wait for response
	do
	{
		while(*((vu32*)0x04000184) & (1 << 8));
	} while(REG_RECV_FIFO != 0x55AAAA55);
	//REG_VRAMCNT_C = VRAM_C_ARM9;
	//REG_VRAMCNT_CD = VRAM_CD_ARM9;
	//invalidate
	//dc_invalidate_all();
	
	//void * buffer is in VRAM...
	//fetched sectors are in MAIN_MEMORY_ADDRESS_SDCACHE, must be dma'd back to buffer
	dc_flush_range((void*)MAIN_MEMORY_ADDRESS_SDCACHE, numSectors * 512);
	memcpy((uint16_t*)buffer, (uint16_t*)MAIN_MEMORY_ADDRESS_SDCACHE, (numSectors * 512));	
}

//buffer should be in main memory
PUT_IN_VRAM __attribute__((noinline)) __attribute__((aligned(4)))	void write_sd_sectors_safe(sec_t sector, sec_t numSectors, const void* buffer){

	//remote procedure call on arm7
	//assume buffer is in the vram cd block
	//uint32_t arm7_address = ((uint32_t)buffer) - ((uint32_t)vram_cd) + 0x06000000;
	//dc_wait_write_buffer_empty();
	//dc_invalidate_all();
	//dc_flush_range(vram_cd, 256 * 1024);
	//dc_flush_all();
	//map cd to arm7
	//REG_VRAMCNT_CD = VRAM_CD_ARM7;
	//REG_VRAMCNT_C = VRAM_C_ARM7;
	
	//void * buffer is in VRAM...
	//fetched sectors are in MAIN_MEMORY_ADDRESS_SDCACHE, must be dma'd back to buffer
	dc_flush_range((void*)MAIN_MEMORY_ADDRESS_SDCACHE, numSectors * 512);
	memcpy((uint16_t*)MAIN_MEMORY_ADDRESS_SDCACHE, (uint16_t*)buffer, (numSectors * 512));
	
	REG_SEND_FIFO = 0xAA5500F0;
	REG_SEND_FIFO = sector;
	REG_SEND_FIFO = numSectors;
	REG_SEND_FIFO = (uint32_t)MAIN_MEMORY_ADDRESS_SDCACHE;	//original: void * buffer -> arm7_address
									 //wait for response
	do
	{
		while (*((vu32*)0x04000184) & (1 << 8));
	} while (REG_RECV_FIFO != 0x55AAAA55);
	//REG_VRAMCNT_C = VRAM_C_ARM9;
	//REG_VRAMCNT_CD = VRAM_CD_ARM9;
	//invalidate
	//dc_invalidate_range(buffer, numSectors * 512);
	//dc_invalidate_all();
}
#endif

#endif

//DldiRelocatedAddress = the new DLDI code will be relocated and copied to such target address
//dldiSourceInRam = DLDI source in EWRAM detected by the DLDI patcher
void fixAndRelocateDLDI(u32 dldiSourceInRam){
	#ifdef ARM7
	u32 DldiRelocatedAddress = (u32)0x03808000;
	DLDIARM7Address = (u32*)DldiRelocatedAddress;
	#endif
	#ifdef ARM9
	u32 DldiRelocatedAddress = (u32)0x06800000;
	#endif
	bool clearBSS = false;
	bool patchStatus = dldiRelocLoader(clearBSS, DldiRelocatedAddress, dldiSourceInRam);
	if(patchStatus == true){
		*((vu32*)0x06202000) = 0x4b4f5450; //PTOK
	}
	else{
		*((vu32*)0x06202000) = 0x52455450; //PTER	
		while(1==1);
	}
}

#ifdef ARM9
PUT_IN_VRAM
#endif
bool dldiRelocLoader(bool clearBSS, u32 DldiRelocatedAddress, u32 dldiSourceInRam)
{
	addr_t memOffset;			// Offset of DLDI after the file is loaded into memory
	addr_t patchOffset;			// Position of patch destination in the file
	addr_t relocationOffset;	// Value added to all offsets within the patch to fix it properly
	addr_t ddmemOffset;			// Original offset used in the DLDI file
	addr_t ddmemStart;			// Start of range that offsets can be in the DLDI file
	addr_t ddmemEnd;			// End of range that offsets can be in the DLDI file
	addr_t ddmemSize;			// Size of range that offsets can be in the DLDI file

	addr_t addrIter;

	data_t *pDH;
	data_t *pAH;

	size_t dldiFileSize = 0;
	
	// Target the DLDI we want to use as stub copy and then relocate it to a DldiRelocatedAddress address
	DLDI_INTERFACE* dldiInterface = (DLDI_INTERFACE*)DldiRelocatedAddress;
	pDH = (data_t*)dldiInterface;
	pAH = (data_t *)dldiSourceInRam;
	
	dldiFileSize = 1 << pAH[DO_driverSize];

	// Copy the DLDI patch into the application
	dmaCopyWords(0, (void*)pAH, (void*)pDH, dldiFileSize);
	
	if (*((u32*)(pDH + DO_ioType)) == DEVICE_TYPE_DLDI) {
		// No DLDI patch
		return false;
	}
	
	if (pDH[DO_driverSize] > pAH[DO_allocatedSpace]) {
		// Not enough space for patch
		return false;
	}
	
	memOffset = DldiRelocatedAddress;	//readAddr (pAH, DO_text_start);
	if (memOffset == 0) {
			memOffset = readAddr (pAH, DO_startup) - DO_code;
	}
	ddmemOffset = readAddr (pDH, DO_text_start);
	relocationOffset = memOffset - ddmemOffset;

	ddmemStart = readAddr (pDH, DO_text_start);
	ddmemSize = (1 << pDH[DO_driverSize]);
	ddmemEnd = ddmemStart + ddmemSize;

	// Remember how much space is actually reserved
	pDH[DO_allocatedSpace] = pAH[DO_allocatedSpace];
	
	// Fix the section pointers in the DLDI @ VRAM header
	writeAddr (pDH, DO_text_start, readAddr (pAH, DO_text_start) + relocationOffset);
	writeAddr (pDH, DO_data_end, readAddr (pAH, DO_data_end) + relocationOffset);
	writeAddr (pDH, DO_glue_start, readAddr (pAH, DO_glue_start) + relocationOffset);
	writeAddr (pDH, DO_glue_end, readAddr (pAH, DO_glue_end) + relocationOffset);
	writeAddr (pDH, DO_got_start, readAddr (pAH, DO_got_start) + relocationOffset);
	writeAddr (pDH, DO_got_end, readAddr (pAH, DO_got_end) + relocationOffset);
	writeAddr (pDH, DO_bss_start, readAddr (pAH, DO_bss_start) + relocationOffset);
	writeAddr (pDH, DO_bss_end, readAddr (pAH, DO_bss_end) + relocationOffset);
	
	// Fix the function pointers in the header
	writeAddr (pDH, DO_startup, readAddr (pAH, DO_startup) + relocationOffset);
	writeAddr (pDH, DO_isInserted, readAddr (pAH, DO_isInserted) + relocationOffset);
	writeAddr (pDH, DO_readSectors, readAddr (pAH, DO_readSectors) + relocationOffset);
	writeAddr (pDH, DO_writeSectors, readAddr (pAH, DO_writeSectors) + relocationOffset);
	writeAddr (pDH, DO_clearStatus, readAddr (pAH, DO_clearStatus) + relocationOffset);
	writeAddr (pDH, DO_shutdown, readAddr (pAH, DO_shutdown) + relocationOffset);
	
	if (pDH[DO_fixSections] & FIX_ALL) { 
		// Search through and fix pointers within the data section of the file
		for (addrIter = (readAddr(pDH, DO_text_start) - ddmemStart); addrIter < (readAddr(pDH, DO_data_end) - ddmemStart); addrIter++) {
			if ((ddmemStart <= readAddr(pAH, addrIter)) && (readAddr(pAH, addrIter) < ddmemEnd)) {
				writeAddr (pAH, addrIter, readAddr(pAH, addrIter) + relocationOffset);
			}
		}
	}
	
	
	if (pDH[DO_fixSections] & FIX_GLUE) { 
		// Search through and fix pointers within the glue section of the file
		for (addrIter = (readAddr(pDH, DO_glue_start) - ddmemStart); addrIter < (readAddr(pDH, DO_glue_end) - ddmemStart); addrIter++) {
			if ((ddmemStart <= readAddr(pAH, addrIter)) && (readAddr(pAH, addrIter) < ddmemEnd)) {
				writeAddr (pAH, addrIter, readAddr(pAH, addrIter) + relocationOffset);
			}
		}
	}
	
	if (pDH[DO_fixSections] & FIX_GOT) { 
		// Search through and fix pointers within the Global Offset Table section of the file
		for (addrIter = (readAddr(pDH, DO_got_start) - ddmemStart); addrIter < (readAddr(pDH, DO_got_end) - ddmemStart); addrIter++) {
			if ((ddmemStart <= readAddr(pAH, addrIter)) && (readAddr(pAH, addrIter) < ddmemEnd)) {
				writeAddr (pAH, addrIter, readAddr(pAH, addrIter) + relocationOffset);
			}
		}
	}
	return true;
}