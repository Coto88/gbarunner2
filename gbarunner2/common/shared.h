#ifndef __SHARED_H__
#define __SHARED_H__

//DLDI ARM7 Support (requires to recompile the project)
#define ARM7_DLDI

#define SOUND_BUFFER_SIZE	(8192)
#define SAVE_DATA_SIZE						(0x20000)	//128K SRAM/EEprom/Flash 8bit/16bit/32bit write compatible RAM memory.
#define ROM_DATA_LENGTH 					(0x3A0000 - (32*1024) )	//0x400000 - 0x40000 (hypervisor) - 0x20000 (128K) = 0x3A0000
#define ROM_ADDRESS_MAX						(0x08000000 + ROM_DATA_LENGTH)
#define MAIN_MEMORY_ADDRESS_SAVE_DATA		(0x02400000 - SAVE_DATA_SIZE)	//-> (0x023E0000 ~ 0x023FFFFF) -> mirror: 0x01FE0000
#define MAIN_MEMORY_ADDRESS_SDCACHE			(MAIN_MEMORY_ADDRESS_SAVE_DATA - (32*1024))	//-> (0x23D8000): -- used for shared DLDI sector memory between ARM7/ARM9

#define MAIN_MEMORY_ADDRESS_GBARUNNER_DATA	(MAIN_MEMORY_ADDRESS_SDCACHE)

//Shared work / Saving
#define MAIN_MEMORY_SHWORK					(MAIN_MEMORY_ADDRESS_SDCACHE + (30*1024))

#define save_save_work (MAIN_MEMORY_SHWORK)
#define save_save_work_uncached (save_save_work | 0x800000)
#define save_save_work_state_uncached (save_save_work_uncached + 1)
#define saveWorkCached		((save_work_t *)save_save_work)
#define saveWorkUncached		((save_work_t *)save_save_work_uncached)


//PowerMan bits
#define 	POWMAN_CONTROL_BIT   0
#define 	POWMAN_BATTERY_BIT   1
#define 	POWMAN_AMPLIFIER_BIT   2
#define 	POWMAN_READ_BIT   (1<<7)
#define 	POWMAN_WRITE_BIT   (0<<7)
#define 	POWMAN_SOUND_AMP_BIT   (1<<0)
#define 	POWMAN_SOUND_MUTE_BIT   (1<<1)
#define 	POWMAN_BACKLIGHT_BOTTOM_BIT   (1<<2)	//#define	PM_BACKLIGHT_BOTTOM	(1<<2)
#define 	POWMAN_BACKLIGHT_TOP_BIT   (1<<3)		//#define	PM_BACKLIGHT_TOP	(1<<3)
#define 	POWMAN_SYSTEM_PWR_BIT   (1<<6)			//#define	PM_SYSTEM_PWR		(1<<6)
#define 	POWMAN_SOUND_PWR_BIT   (1<<0)			//#define 	PM_SOUND_PWR 		(1<<0)
#define 	POWMAN_LED_CONTROL(m)_BIT   ((m)<<4)
#define 	POWMAN_LED_ON_BIT   (0<<4)
#define 	POWMAN_LED_SLEEP_BIT   (1<<4)
#define 	POWMAN_LED_BLINK_BIT   (3<<4)
#define 	POWMAN_AMP_OFFSET_BIT   2
#define 	POWMAN_AMP_ON_BIT   1
#define 	POWMAN_AMP_OFF_BIT   0


//----
#define SOUND_EMU_QUEUE_LEN		64

#define address_dtcm (0x02C00000)	//@0x04F00000 @0x01800000
#define MAIN_MEMORY_ADDRESS_ROM_DATA		(0x02040000)

//VRAM Layout
#define sd_cluster_cache_memory (0x06840000)
#define sd_cluster_cache_driver (0x06860000)

#define FIFO_CNT_EMPTY	(1 << 8)
#define REG_FIFO_CNT	(*((vu32*)0x04000184))
#define REG_SEND_FIFO	(*((vu32*)0x04000188))
#define REG_RECV_FIFO	(*((vu32*)0x04100000))

#ifdef __ASSEMBLER__

sd_cluster_cache = sd_cluster_cache_memory
sd_data_base = sd_cluster_cache_driver
sd_is_cluster_cached_table = (sd_data_base + (32*1024))
sd_cluster_cache_info = (sd_is_cluster_cached_table + (16 * 1024))
sd_sd_info = (sd_cluster_cache_info + (256 * 8 + 4))
pu_data_permissions = 0x33600603 @0x33600003 @0x33660003

#endif

#ifndef __ASSEMBLER__

#include <nds.h>

//Saving
#define SAVE_WORK_STATE_CLEAN	0
#define SAVE_WORK_STATE_DIRTY	1
#define SAVE_WORK_STATE_WAIT	2
#define SAVE_WORK_STATE_SDSAVE	3

typedef struct
{
	uint8_t save_enabled;
	uint8_t save_state;
	uint16_t saveramcrc;
	uint32_t saveFileSaveSize;
	u32 saveFileFirstSector;
	u32 sectorTableSaveFile[(128 * 1024)/512];
	
	//gba file
	uint32_t FileSaveSize;
	u32 	FileFirstSector;
	
	//fs shared
	u32 sectorsPerCluster;
} save_work_t;

#ifdef ARM9
#define PUT_IN_VRAM	__attribute__((section(".vram")))
#define ITCM_CODE	__attribute__((section(".itcm"), long_call))
#endif

#define PACKED __attribute__ ((packed))

#endif //__ASSEMBLER__

#endif


